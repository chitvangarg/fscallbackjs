const fs = require("fs")
const path = require("path")

function createRandomJsonFiles(directoryPath, numberOfFiles, callback) {
    // Directory creation using fs module
    fs.mkdir(directoryPath, (err) => {
        if (err) {
            return callback(err)
        }

        let createdFileCount = 0
        const filePaths = []

        // pushing file data
        for (let file = 1; file <= numberOfFiles; file++) {
            const randomData = { dummyKey: Math.random() }

            // Creating path
            const filePath = path.join(directoryPath, `file${file}.json`)
            const fileContent = JSON.stringify(randomData)

            // Asynchronously writing files
            fs.writeFile(filePath, fileContent, (err, data) => {
                if (err) {
                    callback(err)
                }

                filePaths.push(filePath)
                createdFileCount++

                if (createdFileCount === numberOfFiles) {
                    callback(null, filePaths)
                }
            })
        }
    })
}

function deleteFilesSimultaneously(filePaths, callback) {
    let deletedFiles = 0

    let filesDeleted = []

    // unlinks or permanent delete files in Randomjson
    function deleteNextFiles(filepath, index) {
        // base condition
        if (index === filepath.length) {
            return
        }

        // Pushing fs unlink promises to unlinkfilepromises
        fs.unlink(filepath[index], (err) => {
            if (err) {
                callback(err)
            }

            filesDeleted.push(filepath[index])
            deletedFiles++

            if (deletedFiles == filePaths.length) {
                callback(null, filesDeleted)
            }
        })

        deleteNextFiles(filepath, index + 1)
    }

    deleteNextFiles(filePaths, 0)
}

// Actual function modules

function fsProblem1(directoryPath, numberOfFiles) {
    // Creating random file function call
    createRandomJsonFiles(directoryPath, numberOfFiles, (err, filePaths) => {
        if (err) {
            console.error("Error creating random JSON files:", err)
        } else {
            console.log("Random JSON files created:", filePaths)

            // deleting files sequentially
            deleteFilesSimultaneously(filePaths, (err, deletedFilePaths) => {
                if (err) {
                    console.error("Error deleting files:", err)
                } else {
                    console.log("Files deleted successfully:", deletedFilePaths)
                }
            })
        }
    })
}

module.exports = fsProblem1
