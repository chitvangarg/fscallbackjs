const fs = require("fs")

// Step 1: Read the given file lipsum.txt
function readOriginalFile(lipsumFilePath, callback) {
    fs.readFile(lipsumFilePath, "utf8", (err, data) => {
        if (err) {
            return callback(err)
        }
        callback(null, data)
    })
}

// Step 2: Convert the content to uppercase & write to a new file.
function convertToUppercaseAndWrite(data, callback) {
    const uppercaseData = data.toUpperCase()
    const newFilename = "uppercase.txt"

    fs.writeFile(newFilename, uppercaseData, "utf8", (err) => {
        if (err) {
            return callback(err)
        }
        callback(null, newFilename)
    })
}

// Step 3: Read the new file, convert it to lower case, split into sentences, and write to a new file.
function convertToLowercaseAndSplit(data, callback) {
    const lowercaseData = data.toLowerCase()
    const sentences = lowercaseData.split(/[.!?]/).filter(Boolean)
    const newFilename = "sentences.txt"

    fs.writeFile(newFilename, sentences.join("\n"), "utf8", (err) => {
        if (err) {
            return callback(err)
        }
        callback(null, newFilename)
    })
}

// Step 4: Read the new files, sort the content, and write it to a new file.
function sortContentAndWrite(files, callback) {
    const readFilesCount = files.length
    let completedReads = 0
    let fileContents = []

    files.forEach((file) => {
        fs.readFile(file, "utf8", (err, data) => {
            if (err) {
                return callback(err)
            }

            fileContents.push(data)
            completedReads++

            if (completedReads === readFilesCount) {
                const sortedContent = fileContents
                    .join("\n")
                    .split("\n")
                    .sort()
                    .join("\n")
                const newFilename = "sorted.txt"

                fs.writeFile(newFilename, sortedContent, "utf8", (err) => {
                    if (err) {
                        return callback(err)
                    }
                    callback(null, newFilename)
                })
            }
        })
    })
}

// Step 5: Read the contents of filenames.txt and delete all the new files mentioned in that list simultaneously.
function readFilenamesAndDeleteFiles(callback) {
    fs.readFile("filenames.txt", "utf8", (err, data) => {
        if (err) {
            return callback(err)
        }

        const filenames = data.split("\n").filter(Boolean)
        let completedDeletes = 0
        const totalDeletes = filenames.length

        filenames.forEach((filename) => {
            fs.unlink(filename, (err) => {
                if (err) {
                    return callback(err)
                }

                completedDeletes++

                if (completedDeletes === totalDeletes) {
                    callback(null, "All files deleted successfully.")
                }
            })
        })
    })
}

// Execute the steps in sequence using callbacks.

function fsProblem2(lipsumFilePath) {
    readOriginalFile(lipsumFilePath, (err, data) => {
        if (err) {
            console.error("Error reading the original file:", err)
            return
        }

        //callback to covert to upper case
        convertToUppercaseAndWrite(data, (err, uppercaseFile) => {
            if (err) {
                console.error("Error converting to uppercase and writing:", err)
                return
            }

            // adding new filename in filenames.txt
            fs.appendFile(
                "./filenames.txt",
                `${uppercaseFile}\n`,
                "utf-8",
                (err) => {
                    if (err) {
                        console.log(err)
                    }
                }
            )

            // callback to convert uppercasefiledata to lowercase and split eaxh sentence to new line
            convertToLowercaseAndSplit(data, (err, sentencesFile) => {
                if (err) {
                    console.error(
                        "Error converting to lowercase, splitting, and writing:",
                        err
                    )
                    return
                }

                // adding new filename in filenames.txt
                fs.appendFile(
                    "./filenames.txt",
                    `${sentencesFile}\n`,
                    "utf-8",
                    (err) => {
                        if (err) {
                            console.log(err)
                        }
                    }
                )

                const filesToSort = [uppercaseFile, sentencesFile]

                //callback to sort content by sentence
                sortContentAndWrite(filesToSort, (err, sortedFile) => {
                    if (err) {
                        console.error("Error sorting content and writing:", err)
                        return
                    }

                    // adding new filename in filenames.txt
                    fs.appendFile(
                        "./filenames.txt",
                        `${sortedFile}\n`,
                        "utf-8",
                        (err) => {
                            if (err) {
                                console.log(err)
                            }
                        }
                    )

                    // callback to readfilenames in filenames.txt and delete that files
                    readFilenamesAndDeleteFiles((err, message) => {
                        if (err) {
                            console.error("Error deleting files:", err)
                            return
                        }

                        console.log(message)
                    })
                })
            })
        })
    })
}

module.exports = fsProblem2