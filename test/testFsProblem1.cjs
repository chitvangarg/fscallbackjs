const fsProblem1 = require('../fs-problem1.cjs')


// Calling module with absolute path

const randomNumberOfFiles = Math.floor(Math.random()*10)
const absolutePathOfRandomDirectory = `${__dirname}/randomJson`

fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)